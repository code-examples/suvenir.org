# [suvenir.org](https://suvenir.org) source codes

<br/>

### Run suvenir.org on localhost

    # vi /etc/systemd/system/suvenir.org.service

Insert code from suvenir.org.service

    # systemctl enable suvenir.org.service
    # systemctl start suvenir.org.service
    # systemctl status suvenir.org.service

http://localhost:4039
